package Recursivitat

import java.util.*

fun nombreCreixent(n: Int): Boolean {
        if (n < 10) {
            return true
        }
        val lastDigit = n % 10
        val secondLastDigit = n / 10 % 10
        return if (lastDigit <= secondLastDigit) {
            nombreCreixent(n / 10)
        } else {
            false
        }
    }


    fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val resultat = nombreCreixent(n)
    println("El nombre de digits $n es $resultat")
}