package Recursivitat
import java.util.*



fun nombreDeDigits(n: Int): Int {
        return if (n < 10) {
            1
        } else {
            1 + nombreDeDigits(n / 10)
        }
    }

    fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val resultat = nombreDeDigits(n)
    println("El nombre de digits $n es $resultat")
}