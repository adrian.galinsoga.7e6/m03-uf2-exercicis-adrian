package Recursivitat

import java.util.*


//La función factorial toma un parámetro Int n y devuelve un Int que es el factorial de n.
fun dobleFactorial(n: Int): Int {

//Si n és 0 o 1, el factorial és 1, ja que 0 i 1 són tots dos iguals a 1.
    return if (n == 0 || n == 1) {
        1
//Si n és més gran que 1, la funció s'anomena a si mateixa amb n-2 i multiplica el resultat per n.
    } else {
        n * dobleFactorial(n - 2)
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val resultat = dobleFactorial(n)
    println("El doble factorial de $n es $resultat")
}