package Recursivitat

import java.util.*


//La función factorial toma un parámetro Int n y devuelve un Int que es el factorial de n.
fun factorial(n: Int): Int {

//Si n és 0 o 1, el factorial és 1, ja que 0 i 1 són tots dos iguals a 1.
   return if (n == 0 || n == 1) {
            1
//Si n és més gran que 1, la funció s'anomena a si mateixa amb n-1 i multiplica el resultat per n.
   } else {
            n * factorial(n - 1)
        }
    }

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()

    // Calcula el factorial del número ingressat
    val resultat = factorial(n)

    println("El factorial de $n es $resultat")
}



