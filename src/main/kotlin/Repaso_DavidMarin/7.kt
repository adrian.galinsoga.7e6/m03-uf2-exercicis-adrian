package Repaso_DavidMarin

//7. Algoritme que demana un número entre 0 i 10, i dona error si és menor de 0 o major de 10.

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()

    if (num in 0..10) {
        println("El número está entre 0 y 10")
    } else {
        println("Error: El numero no está entre 0 y 10")

    }
}