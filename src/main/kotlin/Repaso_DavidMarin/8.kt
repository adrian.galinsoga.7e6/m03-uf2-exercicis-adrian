package Repaso_DavidMarin

//8. Algoritme que demana una nota entre 0 i 10, i mostra si és un aprovat o un suspés.

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()

    if (num in 0..4) {
        println("Suspés")
    } else if (num in 5..10) {
        println("Aprovat")

    }
}
