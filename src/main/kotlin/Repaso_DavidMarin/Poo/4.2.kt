package Repaso_DavidMarin.Poo


/*4.1 Pastisseria
Volem desenvolupar una petita aplicació que pugui controlar la brioixeria que comercialitza, de tal forma que de cada pasta es tingui el nom, el preu, el pes i les calories aproximades.
Crea la classe pasta per guardar aquesta informació.
Fes un petit programa que crei 3 elements (croissant, ensaimada i donut, amb els preus, pes i calories que vosaltres vulgueu) i els mostri per pantalla.
*/

/*4.2
La pastisseria ha vist potencial al nostre desenvolupament, i ens demana una ampliació. Ara també volen una classe beguda que tingui el nom, el preu, i si té increment per beguda ensucrada (serà cert o fals si en té).
Afegeix les begudes:
Aigua 1,00  false
Café tallat 1,35 false
Té vermell 1,50 false
Cola 1,65 true
Mostra per pantalla les 3 pastes i les 4 begudes.
 */






fun main(){
    val donut = Pasta("Donut", 1.3, 90.0, 154.0)
    val croisant = Pasta ("Coca", 1.3,90.0, 154.0)
    val panini = Pasta ("Coca", 1.390,5.0, 154.0)

    val aigua = Beguda ("Agua", 1.3, false)
    val cocacola = Beguda ("Cocacola", 1.6, true)
    val fanta = Beguda ("Cocacola", 1.6, true)

}
