package Repaso_DavidMarin.Poo
import java.util.*

//Definició dels colors que farem servir, agafant els backgrounds ANSI, i establint-los com a constants globals
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"


//aquest podria ser un cas on les variables globals tenen sentit, ja que qualsevol modificació afecta al funcionament de la làmpada
class Lampara(val id: String) {
    val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    var color = 0
    var offColor = 1
    var intensidad = 0
    var intensidadPujada = true

//Funció que encèn la làmpada si aquesta està apagada, agafant com a color inicial el que es va guardar a l'apagar,  posa la intensitat a 1 i indica que la intensitat anirà de pujada
        fun turnOn() {
        if (color == 0) {
            color = offColor
            intensidad = 1
            intensidadPujada = true
        }
        mostrarLlum()
    }

//Funció que apaga la làmpada si està encesa, guarda el color de la llum per a la propera vegada que s'encengui, posa el color a 0 (negre) per a indicar que està apagada, la intensitat la posa a 0, ja que la bombeta està apagada
    fun turnOff() {
        if (color > 0) {
            offColor = color
            color = 0
            intensidad = 0
        }
        mostrarLlum()
    }


//Funció que modifica el color del llum de la bombeta si la làmpada està encesa (és a dir, el color no és el negre). Si el llum actual és el darrer (cyan), torna al primer (white)
    fun changeColor() {
        if (color != 0) {
            if (color <= colors.size - 2) color++
            else if (color == colors.size - 1) color = 1
        }
        mostrarLlum()
    }


//Funció que modifica la intensitat de la bombeta si és que la làmpada està encesa. Si la intensitat està configurada de pujada (intensitatPujada=true), anirà pujant fins al 5, i si arriba al 5, indicarà que va de baixada (intensitatPujada=false) i anirà de baixada fins a l'1
    fun intensity() {
        if (color != 0) {
            if (intensidadPujada && intensidad < 5) intensidad++
            else if (intensidadPujada && intensidad == 5) {
                intensidadPujada = false
                intensidad--
            } else if (!intensidadPujada && intensidad > 1) intensidad--
            else if (!intensidadPujada && intensidad == 1) {
                intensidadPujada = true
                intensidad++
            }
        }
        mostrarLlum()
    }

    fun mostrarLlum() {
        println("Lampara $id - Color: ${colors[color]}    $ANSI_RESET - intensitat $intensidad")
    }
}

fun main() {
    val lampara1 = Lampara("Comedor")
    val lampara2 = Lampara("Cocina")
    val lampara3 = Lampara("Habitacion")
    val lamparas = arrayOf(lampara1, lampara2, lampara3)


    do {
        println("Seleccione una habitación: \n"+
            "\nCocina" +
            "\nComedor" +
            "\nHabitacion")

        val seleccion = readln().toLowerCase()
        var lampara:Int = 0

        when (seleccion){
            "comedor" -> lampara = 0
            "cocina" -> lampara = 1
            "habitacion" -> lampara = 2
        }

        do {
            print("Introdueix ordre: ")
            val instr = readln().toUpperCase()
            when (instr) {
                "TURN ON" -> lamparas[lampara].turnOn()
                "TURN OFF" -> lamparas[lampara].turnOff()
                "CHANGE COLOR" -> lamparas[lampara].changeColor()
                "INTENSITY" -> lamparas[lampara].intensity()
            }
        } while(instr!="CHANGE BULB")
        println("Quieres salir? (S/N)")
        val salir = readln()
    } while(salir!="S")


//    println("\n---- LAMPARA 1: COMEDOR ----")
//    lampara1.turnOn()
//    repeat(3) { lampara1.changeColor() }
//    while (lampara1.intensidad < 5) lampara1.intensity()
//
//    println("---- LAMPARA 2: COCINA ----")
//    lampara2.turnOn()
//    repeat(2) { lampara2.changeColor() }
//    while (lampara2.intensidad < 5) lampara2.intensity()
//
//
//    println("---- LAMPARA 3: HABITACIÓN ----")
//    lampara3.turnOn()
//    repeat(3) { lampara3.changeColor() }
//    while (lampara3.intensidad < 5) lampara3.intensity()
}
