package Repaso_DavidMarin.Arxius

import java.io.File
import java.util.*




fun main () {
    //ficherosA1()
    //ficherosA2()
    //ficherosB1()
    //ficheroC1()
    ficheroD1()
}



//OPCIÓN 1 LEE LOS NUMEROS (diferentes lineas)
fun ficherosA1 (){

    val fOrigen = File ("/Users/adrian.galinsoga/Desktop/Numbers.txt")

    fOrigen.forEachLine {
    println("Linea: $it")
    }

}


//OPCIÓN 1 CONTAR PARES E IMPARES (diferentes lineas)

fun ficherosA2 (){

    val fOrigen = File ("/Users/adrian.galinsoga/Desktop/Numbers.txt")
    var parells: Int = 0
    var senars: Int = 0

    fOrigen.forEachLine {
    if (it.toInt() % 2 == 0) parells++
    else senars++
    }
    println(parells)
    println(senars)
}



//OPCIÓN 2 numeros separados por espacio
fun ficherosB1() {
    val sc = Scanner(File("/Users/adrian.galinsoga/Desktop/Numbers.txt"))

    var parells = 0
    var senars = 0

    while (sc.hasNextLine()) {
        val line = sc.nextInt()
            if ( line % 2 == 0) parells++
             else senars++
        }
    println(parells)
    println(senars)

    }


    //OPCIÓN 3
    fun ficheroC1() {
        val sc = Scanner(File("/Users/adrian.galinsoga/Desktop/Numbers.txt"))
        var parells = 0
        var senars = 0
        while (sc.hasNextLine()) {
            val line = sc.nextLine().split(",")
            for (i in line.indices) {
                if (line[i].toInt() % 2 == 0) {
                    parells++
                } else senars++
            }
            println(parells)
            println(senars)
        }
    }


//OPCIÓN 4 Escribir en dos archivos diferentes los numeros pares y los impares
fun ficheroD1() {
    val fOrigen = File ("/Users/adrian.galinsoga/Desktop/Numbers.txt")
    var parells: Int = 0
    var senars: Int = 0

    fOrigen.forEachLine {
        if (it.toInt() % 2 == 0) parells++
        else senars++
        val file = File("/Users/adrian.galinsoga/Desktop/NumbersParells.txt")

    }


    }



