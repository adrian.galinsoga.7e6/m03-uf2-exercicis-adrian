package Repaso_DavidMarin.Arxius

import kotlin.io.path.Path
import kotlin.io.path.readLines


fun main() {
        /*
        Arxiu de simulació del trànsit diari de cotxes i altres vehicles per una
        carretera secundària de Montcada i Reixac. Sabent que -de cada fila- la
        primera dada és la data, la segona el número de cotxes i la tercera el
        número "d'altres vehicles":

        Ens demanen:
        1. Número total de cotxes que han passat en el 2022. (resultat: 11807)
        2. Número total d'altres vehicles que han passat en el 2022. (resultat: 11488)
        3. Total de vehicles que han passat en el 2022. (resultat: 23295)
        4. Mitja de cotxes que han passat cada dia del 2022. (resultat: 32.3479)
        5. Mitja d'altres vehicles que han passat cada dia del 2022. (resultat: 31.4739)
        6. ... (inventeu-vos altres dades interessants, com totals mensuals, dia de més afluència de vehicles...).
        7. El usuario introduce un mes y te sale el total del mes
        */




        val transitdata = Path("/src/main/kotlin/Repaso_DavidMarin/Arxius/MiRTransit2022.csv").readLines()


        var totalCoches = 0
        var totalOtros = 0

        println("Introduce un mes:")
        val mesUsuario = readln().toInt()

        var diaMesX = 0
        var totalCochesDelMes = 0
        var totalOtrosDelMes = 0


        for (i in transitdata){
            val datosDia = i.split(",")
            totalCoches += datosDia[1].toInt()
            totalOtros += datosDia[2].toInt()

            val fecha = datosDia[0].split("/")
                if (fecha[1].toInt() == mesUsuario) {
                        diaMesX++
                        totalCochesDelMes += datosDia[1].toInt()
                        totalOtrosDelMes += datosDia[2].toInt()
                }
        }




        val suma = totalOtros + totalCoches
        val mediaCoches = totalCoches/transitdata.size
        val mediaOtros = totalOtros/transitdata.size
        val mediaMensual = suma/30
        val mediaCochesMesX = totalCochesDelMes/diaMesX
        val mediaOtrosMesX = totalOtrosDelMes/diaMesX

        println("Total coches: $totalCoches")
        println("Total otros vehículos: $totalOtros")
        println("Total de coches $suma")
        println("Total media coche: $mediaCoches")
        println("Total media otros: $mediaOtros")
        println("Total media mensual: $mediaMensual")
        println("Total media coche mes $mesUsuario: ${mediaCochesMesX.toDouble()}")
        println("Total media coche mes $mesUsuario: ${mediaOtrosMesX.toDouble()}")

}



