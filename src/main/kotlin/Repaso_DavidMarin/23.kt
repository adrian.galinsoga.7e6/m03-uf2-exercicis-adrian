package Repaso_DavidMarin

//Algoritme igual a l'anterior, però al final diu quants insuficients,
//quants suficients, quants bé, quants notables i quants excel·lents.


import java.util.*

fun main() {

    var insuficient = 0
    var suficient = 0
    var be = 0
    var notable = 0
    var exellent = 0
    var i = 0

    while (i < 10) {
        val scanner = Scanner(System.`in`)
        val num = scanner.nextInt()

        if (num < 5) {
            insuficient++

        }else if (num == 5) {
            suficient++
        }
        else if (num  <= 7){
            be++
        }
        else if (num <= 9){
            notable++
        }
        else{
            exellent++
        }
        i++
    }
    println("Insufi: $insuficient")
    println("Sufi: $suficient")
    println("Be: $be")
    println("Notable: $notable")
    println("Exel·lent: $exellent")
}
