package Repaso_DavidMarin


//Algoritme que demana 10 números (amb un bucle) entre 0 i 10 i al final mostra per pantalla quants d'aquests eren inferiors a 5 i quants iguals o superiors.

import java.util.*




fun main() {

    var menor = 0
    var mayor = 0
    val scanner = Scanner(System.`in`)

    for (i in 0 until 10){

        val num = scanner.nextInt()

        when (num){
            in 1..5 -> menor++
            in 6..10 -> mayor++
        }
    }
println("$menor $mayor")
}

