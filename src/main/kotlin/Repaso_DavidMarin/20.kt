package Repaso_DavidMarin

import java.math.RoundingMode
import java.util.*
fun main() {

    val scanner = Scanner(System.`in`)
    println("Introduce la cantidad")
    val quantitat = scanner.nextDouble()
    println("Introduce el interés %")
    val interes = scanner.nextInt()
    println("Introduce los años")
    val anyos = scanner.nextInt()

    var capitalTotal = quantitat
    var interesAcumulado = 0.0

    for (i in 0..anyos) {
        interesAcumulado = capitalTotal * interes / 100
        var capitalTotal = capitalTotal + interesAcumulado
        println(capitalTotal.toBigDecimal().setScale(2, RoundingMode.UP))
    }
}

//VERSIÓN BÁSICA
/*
    val scanner = Scanner(System.`in`)
    println("Introduce la cantidad")
    val quantitat = scanner.nextDouble()
    println("Introduce el interés %")
    val interes = scanner.nextInt()
    println("Introduce los años")
    val anyos=scanner.nextInt()

    val multiplicacionInteres = quantitat*interes/100
    println("La cantidad $quantitat x interes $interes% = $multiplicacionInteres")

    val interesFinal = multiplicacionInteres*anyos
    println("El interes $interes% x $anyos años es = $interesFinal")

    val resultadoFinal = interesFinal+quantitat
    println("La cantidad final es: $resultadoFinal")



    }

 */

