package Repaso_DavidMarin

//5. Algoritme que demana un preu en € i el torna en £.

import java.util.*
import kotlin.math.roundToInt
import kotlin.math.roundToLong

fun main() {
    println("EJERCICIO 5")
    val scanner = Scanner(System.`in`)
    println("Introduce los €")
    val euros = scanner.nextInt()

    val conversio = euros * 1.13
    println("$conversio£")

}
