package Repaso_DavidMarin

import java.util.*

fun main() {
    val numeroRndom = (1..10).random()
    var exit = false
    val scanner = Scanner(System.`in`)

    while(!exit) {
        val numeroIntroducido = scanner.nextInt()
        if (numeroIntroducido > numeroRndom) println("Es menor")
        else if (numeroIntroducido < numeroRndom) println("Es mayor")
        else {
            print("Lo has adivinado")
            exit = true
        }
    }

}


