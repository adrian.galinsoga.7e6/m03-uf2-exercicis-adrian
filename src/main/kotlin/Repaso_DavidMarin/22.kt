package Repaso_DavidMarin

// 22. Algoritme que demana 10 números (amb un bucle) entre 0 i 10 i al final mostra per
// pantalla quants d'aquests eren inferiors a 5 i quants iguals o superiors.


import java.util.*

fun main() {

    var numInferior = 0
    var numSuperior = 0
    var i = 0

    while (i in 0 .. 10) {
        val scanner = Scanner(System.`in`)
        val num = scanner.nextInt()

        if (num <= 5) {
            numInferior++

        }else if (num > 5) {
            numSuperior++
        }
        i++
    }
    println("Numero Superiores a 5 : $numSuperior")
    println("Numero Inferior o igual a 5: $numInferior")
}
