package Repaso_DavidMarin

import java.util.*

//2. Algoritme que demana dos números i mostra el resultat de la suma, la resta, la multiplicació i la divisió d'ells dos.

fun main() {
    val scanner = Scanner(System.`in`)
    val firstNumber = scanner.nextInt()
    val secondNumber = scanner.nextInt()

    val suma = firstNumber + secondNumber
    val resta = firstNumber - secondNumber
    val multiplicacion = firstNumber * secondNumber
    val division = firstNumber/secondNumber

    println(suma)
    println(resta)
    println(multiplicacion)
    println(division.toDouble())
}

/*SOLUCIÓN DAVID

print ("Introdueix el valor")
val num = readln().toDouble()
val num2 = readln().toDouble()

val suma = num + num2
val resta = num - num2
val multiplicacion = num * num2
val division = num/num2

println(suma)
println(resta)
println(multiplicacion)
println(division.toDouble())


 */