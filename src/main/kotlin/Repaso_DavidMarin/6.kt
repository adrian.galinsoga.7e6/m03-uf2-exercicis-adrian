package Repaso_DavidMarin

//6. Algoritme que demana l'edat, i mostra missatges diferents en funció de si s'és major d'edat o menor.

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val edad = scanner.nextInt()

    if (edad < 18) {
        println("Es menor d'edad")
    } else {
        println("Es mayor d'edad")

    }
}