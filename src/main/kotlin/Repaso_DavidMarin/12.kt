package Repaso_DavidMarin



//Algoritme que mostra la taula de multiplicar.

import java.util.*




fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()
    for (i in 0..10){
        println("$num x $i = ${i * num}")
    }

}

