package Repaso_DavidMarin

// 9. Algoritme que demana una nota entre 0 i 10, i mostra si és un suspés (0-4), un suficient (5), un bé (6), un notable (7-8) o un excel·lent (9-10).

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()

    when (num) {
        in 0..4 -> print("insuficient")
        5 -> print("suficient")
        6 -> print("be")
        7, 8 -> print("notable")
        9, 10 -> print ("excelente")
    }
}

