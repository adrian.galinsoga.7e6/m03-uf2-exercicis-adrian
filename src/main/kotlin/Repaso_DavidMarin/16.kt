package Repaso_DavidMarin

//16. Algoritme igual a l'anterior, mostrant al final el preu menor i el major.


import java.util.*
fun main() {

    var totalPrecio: Double = 0.0
    var menor: Double = 0.0
    var mayor: Double = 0.0

    for (i in 10 downTo 1) {
        println("Introduce el precio del producto")
        val scanner = Scanner(System.`in`)
        val precioProducto = scanner.nextDouble()
        if (i == 1) {
            menor = precioProducto
            mayor = precioProducto

        } else {

            if (precioProducto > mayor) mayor = precioProducto
            if (precioProducto < menor) menor = precioProducto

        }
        totalPrecio = totalPrecio + precioProducto
    }

    val preuMitja: Double = totalPrecio/10.0
    println("El precio medio es de $preuMitja €")
    println("El precio mayor es de $mayor")
    println("El precio menor es de $menor")
}




//SOLUCIÓN CON DUPLICADO DE VARIABLES
/*
fun main() {

    var totalPrecio: Double = 0.0


    val scanner = Scanner(System.`in`)
    val precioProducto = scanner.nextDouble()
    totalPrecio= totalPrecio + precioProducto
    var menor: Double = precioProducto
    var mayor: Double = precioProducto



    for (i in 9 downTo 1) {
        println("Introduce el precio del producto")
        val scanner = Scanner(System.`in`)
        val precioProducto = scanner.nextDouble()
        totalPrecio= totalPrecio + precioProducto
        if (precioProducto > mayor) mayor=precioProducto
        if (precioProducto < menor) menor=precioProducto

    }

    val preuMitja: Double = totalPrecio/10.0
    println("El precio medio es de $preuMitja €")
    println("El precio mayor es de $mayor")
    println("El precio menor es de $menor")
}








 */

