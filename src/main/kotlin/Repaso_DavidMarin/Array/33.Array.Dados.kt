package Repaso_DavidMarin.Array

fun main() {

    val dados = Array(1000) { (1..6).random() }
    println("Tiradas ${dados.joinToString()}")


    val tiradas = arrayOf(0,0,0,0,0,0,0)
    tiradas[1] = dados.count() {it == 1}
    println(tiradas[1])
    tiradas[2] = dados.count() {it == 2}
    println(tiradas[2])
    tiradas[3] = dados.count() {it == 3}
    println(tiradas[3])
    tiradas[4] = dados.count() {it == 4}
    println(tiradas[4])
    tiradas[5] = dados.count() {it == 5}
    println(tiradas[5])
    tiradas[6] = dados.count() {it == 6}
    println(tiradas[6])

    var mayor = tiradas[1]
    var numMayor = 1
    for (i in 1 .. 6){
        if (tiradas [i]> mayor)
            mayor = tiradas[i]
        numMayor = i

    }
    println("$numMayor")
}


