package Repaso_DavidMarin

import java.util.*

fun main() {

    var menor = 0
    var mayor = 0
    val scanner = Scanner(System.`in`)

    for (i in 0 until 10){

        val num = scanner.nextInt()

        when (num){
            in 1..5 -> menor++
            in 6..10 -> mayor++
        }
    }
    println("$menor $mayor")
}

