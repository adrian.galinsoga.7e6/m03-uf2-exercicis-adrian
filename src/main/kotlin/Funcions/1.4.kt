package Funcions

import java.util.*

fun secuencia(paraulas: String, rango1: Int, rango2: Int): String {
    if (rango1 > paraulas.length || rango2 > paraulas.length) return "La subseqüència $rango1 - $rango2 de l'String no existeix"
    else return paraulas.subSequence(rango1, rango2).toString()
}

fun main() {
    val scanner = Scanner(System.`in`)
    val paraulas= scanner.nextLine()
    val rango1 = scanner.nextInt()
    val rango2 = scanner.nextInt()
    println(secuencia(paraulas, rango1, rango2))


}


