package Funcions

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val c = scanner.next().single()
    cruz(n, c)
}

fun cruz(n: Int, c: Char) {
    for (i in 1..n) {
        if (i == n / 2 + 1) {
            repeat(n) {
                print(c)
            }
        } else {
            for (j in 1..n) {
                if (j == n / 2 + 1) {
                    print(c)
                } else print(" ")
            }
        }
        println()
    }
}
