package Funcions

import java.util.*

fun cambioStringAMutableList(paraula: String): MutableList<Char> {
    return paraula.toMutableList()
}

fun main() {
    val scanner = Scanner(System.`in`)
    val paraula = scanner.nextLine()

    val result = cambioStringAMutableList(paraula)
    println(result)
}
