package Funcions

import java.util.*

fun comparativa(paraula1: String, paraula2: String): Boolean {
    return paraula1 == paraula2
}

fun main() {
    val scanner = Scanner(System.`in`)
    val paraula1 = scanner.next()
    val paraula2 = scanner.next()

    val result = comparativa(paraula1, paraula2)
    println(result)

}


