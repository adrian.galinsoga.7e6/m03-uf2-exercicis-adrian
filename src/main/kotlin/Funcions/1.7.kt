package Funcions

import java.util.*


fun elevarNum (primerNum: Int, segonNum: Int): Double {
    return Math.pow(primerNum.toDouble(), segonNum.toDouble())
}


fun main() {
    val scanner = Scanner(System.`in`)
    val primerNum = scanner.nextInt()
    val segonNum = scanner.nextInt()
    val resultat = elevarNum(primerNum, segonNum)
    println(resultat)
}

