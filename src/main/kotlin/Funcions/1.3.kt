package Funcions

import java.util.*

fun posicion(paraulas: String, numero: Int): String {
     if (numero > paraulas.length) return "La mida de l'String és inferior a $numero"
     else return paraulas[numero].toString()
}

fun main() {
    val scanner = Scanner(System.`in`)
    val paraulas= scanner.nextLine()
    val numero = scanner.nextInt()
    println(posicion(paraulas, numero))

}


