package Funcions

import java.util.*

fun length(paraula: String): Int {
    return paraula.length
}

fun main() {
    val scanner = Scanner(System.`in`)
    val paraula = scanner.nextLine()
    println("La longitud de la cadena '$paraula' es: ${length(paraula)}")
}


