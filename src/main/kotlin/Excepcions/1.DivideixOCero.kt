package Excepcions

import java.util.*

fun main (){
    println(
         """
************         
| EXEPTION |
************
                                       
    """
    )

    println("* AUTHOR: Adrian Galinsoga Egea\n" +
            "* DATE: 14/3/23\n" +
            "* TITLE: Divideix o Cero")
    println("________________________________________________________________________________________________________________________________________________________________\n")
    println("* DESCRIPCIÓ:\n"+
            "Volem desenvolupar una funció de nom divideixoCero, que rebi dos paràmetres (el dividend i el divisor)\n" +
            "i retorni un sencer resultat de la divisió, o 0 en cas que el denominador sigui 0. \n"  +
            "No es pot fer servir cap condicional. Excepció: ArithmeticException")
    println("________________________________________________________________________________________________________________________________________________________________\n")


    val scanner = Scanner(System.`in`)
    println("Divident")
    val num1 = scanner.nextInt()
    println("Divisor")
    val num2 = scanner.nextInt()
    println("${divideixoCero(num1, num2)}")
}

fun divideixoCero(num1: Int, num2: Int): Int {
    var resultat = 0

    try {
        resultat = num1 / num2
    } catch(e:ArithmeticException){
        println("0")
    }
    println("Resultat")
    return resultat
}






