package Col_leccions

fun main() {
    val numCarteles = readLine()!!.toInt()

    // Crear el diccionario de carteles
    val carteles = mutableMapOf<Int, String>()
    repeat(numCarteles) {
        val (metro, texto) = readLine()!!.split(" ")
        carteles[metro.toInt()] = texto
    }

    val numConsultas = readLine()!!.toInt()

    // Realizar las consultas
    repeat(numConsultas) {
        val metro = readLine()!!.toInt()
        if (metro in carteles) {
            println(carteles[metro])
        } else {
            println("no hay cartel")
        }
    }
}
