import java.time.LocalDate
import java.util.*
import kotlin.math.*
fun main() {


    val scanner = Scanner(System.`in`).useLocale(Locale.UK)

    val anyo = scanner.nextInt()
    if (anyo % 4 == 0 && (anyo % 100 != 0 || anyo % 400 == 0))
        println("És de traspas")

    else println("No és de traspas")



}