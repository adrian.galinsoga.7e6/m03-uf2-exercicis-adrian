
import java.util.*


fun main() {

    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    var numero = scanner.nextInt()
    var alreves = 0
    while (numero != 0) {
        val digito = numero % 10 //el residuo se almacena en digito
        alreves = alreves * 10 + digito //se almacena en alrevés y luego va añadiéndose en orden
        numero /= 10 //se divide entre 10 para seguir con el loop y que no vuelva a almacenar el mismo digito
    }
    println("El nombre al revés és: $alreves")


}


