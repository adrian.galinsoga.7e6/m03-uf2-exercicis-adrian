/* AUTHOR: Adrian Galinsoga Egea
* DATE: 22/12/22
* TITLE: Exercicis UF1 1.4 Calcula l’àrea
* DESCRIPCIÓ: Una web d'habitatges de lloguer ens ha proposat una ampliació. Volen mostrar l'àrea de les habitacions per llogar. Fes un
programa que ens ajudi a calcular les dimensions d'una habitació. Llegeix l'amplada i la llargada en metres (enters) i mostra'n
l'àrea.
*
*/

import java.util.*
fun main() {


    val scanner = Scanner(System.`in`)
    val primerValor = scanner.nextInt()
    val segonValor = scanner.nextInt()


    val resultat = primerValor*segonValor


    print (resultat)

}



