
/* AUTHOR: Adrian Galinsoga Egea
* DATE: 22/12/22
* TITLE: Exercicis UF1 1.5 Operació boja
* DESCRIPCIÓ: L'usuari escriu 4 enters i s'imprimeix el valor de sumar el primer amb el segon, multiplicat per el mòdul del tercer amb el quart.
*
*/

import java.util.*
fun main() {


    val scanner = Scanner(System.`in`)
    val a = scanner.nextInt()
    val b = scanner.nextInt()
    val c = scanner.nextInt()
    val d = scanner.nextInt()


    val resultat = (a + b) * (c % d)


    print (resultat)

}



