import java.time.LocalDate
import java.util.*
import kotlin.math.*
fun main() {


    val scanner = Scanner(System.`in`).useLocale(Locale.UK)

    val letra = scanner.next().single()


    if (letra == 'a' || letra == 'e' || letra== 'i' || letra== 'o' || letra =='u')
        println("Vocal")

    if (letra == 'A' || letra == 'E' || letra== 'I' || letra== 'O' || letra =='U')
        println("Vocal")

    else println("Consonant")

}