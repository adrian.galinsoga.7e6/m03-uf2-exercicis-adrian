/* AUTHOR: Adrian Galinsoga Egea
* DATE: 22/12/22
* TITLE: Exercicis UF1 1.10 Quina és la mida de la meva pizza?
* DESCRIPCIÓ: Llegeix el diàmetre d'una pizza rodona i imprimeix la seva superfície. Pots usar Math.PI per escriure el valor de Pi.
*
*/

import java.util.*
fun main() {


    val scanner = Scanner(System.`in`)
    val diametro = scanner.nextDouble()



    val radio = diametro/2
    val superficie = radio*diametro/2*3.14


    print ("$superficie")

}



