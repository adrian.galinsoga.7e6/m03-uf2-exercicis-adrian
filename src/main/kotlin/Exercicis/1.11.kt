/* AUTHOR: Adrian Galinsoga Egea
* DATE: 22/12/22
* TITLE: Exercicis UF1 1.11 Calculadora de volum d’aire
* DESCRIPCIÓ: Per poder fer un estudi de la ventilació d'una aula necessitem poder calcular la quantitat d'aire que hi cap en una habitació.
Llegeix les 3 dimensions de l'aula i imprimeix per pantalla quin volum té.
*
*/

import java.util.*
fun main() {


    val scanner = Scanner(System.`in`)
    val llargada = scanner.nextDouble()
    val ample = scanner.nextDouble()
    val alçada = scanner.nextDouble()


    var volum = llargada*ample*alçada


    print ("$volum")

}



