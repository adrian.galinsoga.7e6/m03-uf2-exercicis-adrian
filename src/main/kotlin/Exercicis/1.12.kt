/* AUTHOR: Adrian Galinsoga Egea
* DATE: 22/12/22
* TITLE: Exercicis UF1 1.12 De Celsius a Fahrenheit
* DESCRIPCIÓ: Feu un programa que rebi una temperatura en graus Celsius i la converteixi en graus Fahrenheit.
*
*/

import java.util.*
fun main() {


    val scanner = Scanner(System.`in`)
    val celsius = scanner.nextDouble()


    val fahrenheit = celsius*1.8+32


    print ("$fahrenheit")

}



