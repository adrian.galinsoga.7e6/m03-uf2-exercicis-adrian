/* AUTHOR: Adrian Galinsoga Egea
* DATE: 29/09/22
* TITLE: Exercicis UF1 2.18
*
*
*/


import java.util.*
fun main() {


    val scanner = Scanner(System.`in`)
    val a = scanner.nextInt()


    when (a) {
        !in 1..1000 -> {
            println(a * -1)
        }

        else -> {
            println(a)
        }

    }
}

