/* AUTHOR: Adrian Galinsoga Egea
* DATE: 22/12/22
* TITLE: Exercicis UF1 1.9 Calcula el descompte
* DESCRIPCIÓ: Llegeix el preu original i el preu actual i imprimeix el descompte (en %).
*
*/

import java.util.*
fun main() {


    val scanner = Scanner(System.`in`)
    val preuOriginal = scanner.nextDouble()
    val preuActual = scanner.nextDouble()





    val descuento = (preuActual-preuOriginal) / preuOriginal * 100


    print ("$descuento%")

}



