package POO

class Beguda(val nom: String, var preu: Double, val sucre: Boolean) {
    fun drinks(){
        if (sucre==true) preu += preu*0.1
        println("Producte: ${nom}, Preu: ${preu}€, Sucre: ${sucre}")
    }


}
    val aigua = Beguda("Bezoya", 1.0, false)
    val cola = Beguda("Coca-Cola", 2.35, true)
    val cafe = Beguda("Café tallat", 1.50, false)
    val te = Beguda("Vermell", 1.65, true)


fun main() {
    //Pastes
    println("Producte: ${croissant.nom}: Preu: ${croissant.preu}€, Pes: ${croissant.pes}G, Calories: ${croissant.calories}Kcal")
    println("Producte: ${ensaimada.nom}: Preu: ${ensaimada.preu}€, Pes: ${ensaimada.pes}G, Calories: ${ensaimada.calories}Kcal")
    println("Producte: ${donut.nom}: Preu: ${donut.preu}€, Pes: ${donut.pes}G, Calories: ${donut.calories}Kcal")

    //Begudes

    aigua.drinks()
    println("Producte: ${cola.nom}, Preu: ${cola.preu}€, Sucre: ${cola.sucre}")
    cola.drinks()
    println("Producte: ${cafe.nom}, Preu: ${cafe.preu}€, Sucre: ${cafe.sucre}")
    cafe.drinks()
    println("Producte: ${te.nom}, Preu: ${te.preu}€, Sucre: ${te.sucre}")
    te.drinks()
}

