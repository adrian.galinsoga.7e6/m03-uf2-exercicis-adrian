package POO

class pasta (val nom: String, val preu: Double, val pes: Int, val calories: Int)
val croissant = pasta("Croissant", 1.5, 50, 250)
val ensaimada = pasta("Ensaimada", 2.0, 80, 350)
val donut = pasta("Donut", 1.0, 40, 200)

fun main(){
    println("Producte: ${croissant.nom}, Preu: ${croissant.preu}€, Pes: ${croissant.pes}G, Calories: ${croissant.calories}Kcal")
    println("Producte: ${ensaimada.nom}, Preu: ${ensaimada.preu}€, Pes: ${ensaimada.pes}G, Calories: ${ensaimada.calories}Kcal")
    println("Producte: ${donut.nom}, Preu: ${donut.preu}€, Pes: ${donut.pes}G, Calories: ${donut.calories}Kcal")
}